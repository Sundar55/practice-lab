<center>
<img src="images/IDSN-new-logo.png" width = "300">
</center>

# Practice Project - Create a CRUD Rest API

## Estimated Effort: 1 Hr 30 mins

## Objectives: 

In the CRUD lab we learnt to create API endpoints for performing Create, Retrieve, Update and Delete operations on transient data with express server using "query" and "params" from the HTTP request.

- In this lab, we will be doing the same using "body" from the HTTP request which will retrive the JSON output.

- Access CRUD endpoints and test CRUD operation using CURL and Postman.

<br><br>

## Set-up : Create application
1. Open a terminal window by using the menu in the editor: Terminal > New Terminal.

<img src="images/new-terminal.png" width="75%"/>


2. Change to your project folder, if you are not in the project folder already.

```
cd /home/project
```

{: codeblock}


3. Run the following command to clone the git repository that contains the starter code needed for this lab, if it doesn't already exist.

```
[ ! -d 'Practice-lab' ] && git clone https://github.com/sapthashree15/Practice-lab.git
```
{: codeblock}

<img src="images/git-clone.jpg" width="75%"/>

5. Change to the directory **Practice-lab** directory to start working on the lab.

```
cd Practice-lab/
```

{: codeblock}

<img src="images/cd_to_correct_folder.jpg" width="75%"/>

6.  List the contents of this directory to see the artifacts for this lab.

```
ls
```
{: codeblock}

<img src="images/ls_command.jpg" width="75%"/>

#

## Exercise 1: Accessing the basic Express app:

1.  In the files explorer open the **Practice-lab** folder and view **index.js**.

<img src="images/ex1-index.js.jpg" width="75%"/>

It will have the following content:

```
const express = require('express');

const routes = require('./router/practiceuser.js')


const app = express();

const PORT =5000;

app.use(express.json());
app.use("/user", routes);

app.listen(PORT,()=>console.log("Server is running"));
```

This basically means that your express server has been configured to run at port 5000. When you access the server with **/user** you can access the end points defined in **router/practiceuser.js**. You have to install two packages, namely express and nodemon, before you can run your server.

2. In the terminal window, run the following command to install the **express** package.

```
npm install express --save
```
{: codeblock}

<img src="images/ex1-express_installation.jpg" width="75%"/>

3. Run the following command to install the **nodemon** package is installed. 

```
npm install nodemon --save
```
{: codeblock}

<img src="images/ex1-nodemon_installation.jpg" width="75%"/>

<br><br>

#

## Exercise 2: Develop your Express app:

1. Navigate to the file named `practiceusers.js` in the `router` folder. It has some starter code provided already as seen in the image below.

<img src="images/ex2-practiceusers.js.jpg" width="75%"/>

<br><br>

2. **R** in CRUD stands for retrieve. You will first add a GET API endpoint, using the **get** method for getting the details of all users. A few users have been added in the starter code. Copy the code below and paste in users.js.

```

router.get('/',function (req, res) {

   res.send(JSON.stringify({users}, null, 4));

});
```
{: codeblock}

<img src="images/Ex3-GET_method.jpg" width="75%"/>

4. To test this endpoint, in the terminal window run the server with the following command:

```
npm start 
```

<img src="images/Ex3-npm_start.jpg" width="75%"/>


5. Click on the Skills Network button on the right, it will open the "Skills Network Toolbox". Then click `OTHER` then `Launch Application`. From there you should be able to enter the port as `5000` and launch the development server.

<img src="images/Launch_Application.png" width="80%" style="border: solid 1px grey"/>

6. When the browser page opens up, suffix **/user** to the end of the URL on the address bar.You will see the below page.

<img src="images/ex2-GET-output.jpg" width="75%"/>

7. We can check the output of the GET request using curl command 

```
curl localhost:5000/user/
```
{: codeblock}

<br><br>

## Exercise 3: Creating a GET by specific ID method:

1. Create a GET method for getting the details of a specific user based on his/her email ID by using the `filter` method to filter a specific user by `email`.


<details><summary>You can also click here to view the code</summary>

```
router.get("/:email",function (req,res){
    const email = req.params.email;
    let filtered_users = users.filter((user) => user.email === email);
    res.json(filtered_users);
    
});
```

<img src="images/Ex3-GET_by_specific_ID.jpg" width="75%"/>

</details>


2. In the terminal window run the server with the following command:

```
npm start 
```

<img src="images/Ex3-npm_start.jpg" width="75%"/>


3. Click on Terminal > Split Terminal

<img src="images/ex2-split_terminal.png" width="75%"/>

Open a new terminal & use the below command to view the output for the user with mail id 'curl localhost:5000/user/annasmith@gamil.com':

```
curl localhost:5000/user/annasmith@gamil.com
```

<img src="images/Ex3-GET_by_specific_ID-CURL.jpg" width="75%"/>


## Exercise 4: Creating the POST method:

1. The **/user** endpoint with POST method is used for adding a user to the list. We send the user details as query parameters with the `push` function for creating a POST method for adding a new user with the details - `firstName`, `lastName`, `DOB` and `email` & pushing it back to the `users` array

```
router.post("/",function (req,res){
    if(!req.body.email){
        res.status(400)
        return res.json({error: "email is required..."})
    }

    const user ={
        "firstName":req.body.firstName,
        "lastName":req.body.lastName,
        "email":req.body.email,
        "DOB":req.body.DOB
    }

    users.push(user);
    res.send("The user" + (' ')+ (req.body.firstName) + " Has been added!");
})
```

2. The completed code will look like this.

<img src="images/Ex4-POST.jpg" width="75%"/>

3. Use the below command to post a new user with mail id 'robertwhite@gamil.com' on the split terminal:

```
curl --location --request POST 'https://sapthashreek-5000.theiadocker-3-labs-prod-theiak8s-4-tor01.proxy.cognitiveclass.ai/user' \
--header 'Content-Type: application/json' \
--header 'Cookie: jhub-reverse-tool-proxy=s%3A5c7173fd-d64a-4d75-bb49-a91a7e272ecd.atdgasa1DhGnbvrhOKuogJKbgAIQO4%2FidvgMGXj6b7c' \
--data-raw '{
        "firstName": "Robert",
        "lastName": "White",
        "email": "robertwhite@gamil.com",
        "DOB": "02-03-1993"
    }'
```

4. The ouput will be as below:

<img src="images/Ex4-POST-CURL1.jpg" width="75%"/>

<br><br>

5. To verify if the user with email 'robertwhite@gamil.com' has been added, you can send a GET request as below:

```
curl localhost:5000/user/robertwhite@gamil.com
```

<img src="images/Ex4-GET_request-after-POST.jpg" width="75%"/>

## Exercise 5: Creating the PUT method:

1. The **U** in CRUD stands for update which can be achieved using the PUT method. To make updates in the data, we use PUT method. We first look for the object based on the email and then update one specific details about that users. 

```
router.put("/:email", function (req, res) {
    const email = req.params.email;
   
    let filtered_users = users.filter((user) => user.email === email);
   
    if (filtered_users.length > 0) {
        let filtered_user = filtered_users[0];
        let DOB = req.body.DOB;
        let firstName = req.body.firstName;
        let lastName = req.body.lastName;
        if(DOB) {
            filtered_user.DOB = DOB
        }
        if(firstName) {
            filtered_user.firstName = firstName
        }
        if(lastName) {
            filtered_user.lastName = lastName
        }
        users = users.filter((user) => user.email != email);
        users.push(filtered_user);
        res.send(`User with the email  ${email} updated.`);
    }
    else{
        res.send("Unable to find user!");
    }
  });
  ```
2. The completed code will look like this.

<img src="images/Ex4-PUT_full_code.jpg" width="75%"/>

3. Use the below command to update the `DOB` as `02-03-1995` for the user with mail id 'robertwhite@gamil.com' in the split terminal:

```
curl --location --request PUT 'https://XXXXXXX-5000.theiadocker-3-labs-prod-theiak8s-4-tor01.proxy.cognitiveclass.ai/user/robertwhite@gamil.com' \
--header 'Content-Type: application/json' \
--header 'Cookie: jhub-reverse-tool-proxy=s%3A5c7173fd-d64a-4d75-bb49-a91a7e272ecd.atdgasa1DhGnbvrhOKuogJKbgAIQO4%2FidvgMGXj6b7c' \
--data-raw '{
    "DOB":"02-03-1995"
}'
```

4. The ouput will be as below:

<img src="images/ex5-PUT-CURL.jpg" width="75%"/>

<br><br>

5. To verify if the `DOB' of the user with email 'robertwhite@gamil.com' has been updated, you can send a GET request as below:

```
curl localhost:5000/user/robertwhite@gamil.com
```

<img src="images/ex5-GET_request-after-PUT.jpg" width="75%"/>

## Exercise 6: Creating the DELETE method:

1. Create a DELETE method for deleting specific users by email by using the below code:

```
router.delete("/:email", (req, res) => {
    const email = req.params.email;
    users = users.filter((user) => user.email != email);
    res.send(`User with the email  ${email} deleted.`);
  });
```
2. The completed code will look like this.
<img src="images/Ex6-DELETE_full_code.jpg" width="75%"/>

3. Use the below command to delete the user with mail id 'robertwhite@gamil.com' in the split terminal:

```
curl --location --request DELETE 'https://XXXXXXX-5000.theiadocker-3-labs-prod-theiak8s-4-tor01.proxy.cognitiveclass.ai/user/robertwhite@gamil.com' \
--header 'Cookie: jhub-reverse-tool-proxy=s%3A5c7173fd-d64a-4d75-bb49-a91a7e272ecd.atdgasa1DhGnbvrhOKuogJKbgAIQO4%2FidvgMGXj6b7c'
```

4. The ouput will be as below:

<img src="images/ex6-DELETE-CURL.jpg" width="75%"/>

5. Send a GET request for the user with email 'robertwhite@gamil.com' and ensure that a null object is returned:

<img src="images/ex6-_GET_request_after_DELETE.jpg" width="75%"/>

<br><br>

## Exercise 7: Testing the output of the above methods:

We can also test these methods, by use Postman as below.

> Note: You can refer to <a href = "https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-CD0201EN-SkillsNetwork/labs/5_RestArchitecture/Instructions_CloudantWithRESTAPI.md.html?origin=www.coursera.org"> this </a> lab to get familiar with and understand how to send requests through Postman.

Go to <a href="https://web.postman.co">Postman</a> and create a new login or sign in with your Google mail credentials login. 

> Note: Make sure that your server is running and the Server is listening on port 5000. 

HTTP GET Request. 
 
1. <b>GET request</b>


a. The output will be as below:

<img src="images/ex7-GET_request-output.jpg" width="75%"/>
<br><br>

2. <b>GET request by specific ID</b>

The output will be as below:

<img src="images/ex7-GET_request-output1.jpg" width="75%"/>

<br><br>

3. <b>POST request : </b>

The output will be as below:

<img src="images/ex7-POST_send.jpg" width="75%"/>

Verify that the newly added values are been updated by doing the GET request.

<br><br>

4. <b>PUT request: </b>

The output will be as below:

<img src="images/ex7-PUT_send.jpg" width="75%"/>


Verify that the newly added values are been updated by doing a GET request.

<br><br>

5. <b>DELETE Request: </b>

The output will be as below:

<img src="images/ex7-DELETE-send.jpg" width="75%"/>

Verify that the GET user by ID `robertwhite@gamil.com` returns a null object by sending a GET request.


<br><br>

### Congratulations! You have completed the practice project for Creating a CRUD Rest API and tested them using CURL and Postman. 

## Summary: 

In this lab, we created a CRUD Rest API and tested it with CURL and Postman.


## Author(s)
<h4> Sapthashree K S <h4/>
<h4> K Sundararajan <h4/>


## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 05-09-2022 | 1.0  | Sapthashree K S | Initial version created |


## <h3 align="center"> (C) IBM Corporation 2020. All rights reserved. <h3/>
